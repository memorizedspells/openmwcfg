# OpenMW configs (WIP)

## Intro

This is a set of configurations I am currently tinkering with, and do not generally contain helpful links to downloads.
Most of these mods can be located by searching for them on [modding-openmw.com](https://modding-openmw.com), or on the Morrowind Nexus, but some are found elsewhere.

Don't do what I did and install a healthy set of 20 or so mods before looking up best practices on installing mods with OpenMW! You'll end up at some point spending a few hours tracking down mods and reinstalling everything because you'll realize you have to fit a mod in between two other mods. OpenMW allows you to install mods in a modular way, so do that!

This mod list was done on a series of OpenMW builds, most recently `c7f8a4faac` from about a day before the 0.47 RC build.

At some point, I hope to clean this up considerably.

## Starting Out

[**modding-openmw** is an indespensible resource](https://modding-openmw.com/getting-started/)

- OpenMW 0.47 (latest)
- ReShade 3.4.1 (or whatever works) (I think this may be Windows-only)
  - I'm using the one from this [mediafire link](http://www.mediafire.com/file/au4lvk667c1ds1l/ReShade_Setup_3.4.1.exe/file)
  - Wait to install it until you download the Reshade settings file below, and download the shaders required.
  - If you want to tinker with the settings, you have to start the game as admin or the settings will be lost as soon as you close.
- [Delta Plugin](https://modding-openmw.com/mods/delta-plugin/)
- [OMLLF](https://modding-openmw.com/mods/omwllf/)

## Configs

- [Mod List by way of openmw.cfg](https://gitlab.com/memorizedspells/openmwcfg/-/blob/main/modlist.md)

- [OpenMW user settings.cfg](https://gitlab.com/memorizedspells/openmwcfg/-/blob/main/settings.cfg)

- [MyReshadeSettings.ini](https://gitlab.com/memorizedspells/openmwcfg/-/blob/main/MyReshadeSettings.ini)


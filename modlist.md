# Mod List

This is my OpenMW modlist, as taken more or less directly out of my `openmw.cfg` file. I'll tidy this up later.

NOTE: You can't do EOL comments in `opemmw.cfg`. I added them so I could sort explain some of what is going on here: 

```python (just for md syntax highlighting, not a python file)
groundcover=GrassZ10.esp
# ... A lot of lines go here,
# to be uploaded at a later date.
# Most of which can be determined
# by reading the pages for mods on
# modding-openmw.com
# ...
data="STEAMDIR\Steam\steamapps\common\Morrowind\Data Files"
data="MWMODDIR\PatchForPurists" # Doesn't touch exploits/balance, but does fix bugs
data="MWMODDIR\ImprovedJournalEntriesV2" # Tells you who sent you on which quest, and where they are.
data="MWMODDIR\UMOPP"           # Unofficial Morrowind Official Plugins Patched
data="MWMODDIR\ExpansionDelay"  # Delays Dark Brotherhood attacks until a reasonable time
data="MWMODDIR\DubdillaLocationFix"
## Fixing / Improving Meshes
data="MWMODDIR\MeshFixV127"
data="MWMODDIR\CorrectMeshes"
data="MWMODDIR\CorrectUVRocks"
data="MWMODDIR\RopeFenceFix"    # Fix Those Bastard Rope Fences - Makes some collision meshes incl. people's smaller and less annoying.
data="MWMODDIR\MOP\00 Core"     # Morrowind Optimization Project
data="MWMODDIR\MOP\01 Fixed Vanilla Textures"
data="MWMODDIR\MOP\02 Lake Fjalding Anti-Suck"
data="MWMODDIR\MOP\05 TreeFix"
data="MWMODDIR\ProjectAtlas\00 Core"
data="MWMODDIR\ProjectAtlas\10 Glow in the Dahrk Patch - Interior Sunrays"
data="MWMODDIR\ProjectAtlas\20 BC Mushrooms - Smoothed"
data="MWMODDIR\ProjectAtlas\30 Redware - Smoothed"
data="MWMODDIR\ProjectAtlas\40 Urns - Smoothed"
data="MWMODDIR\ProjectAtlas\50 Wood Poles - Hi-Res Texture"
data="MWMODDIR\Graphic Herbalism Patches and Replacers\12 Atlas - Smoothed BC Mushrooms"
data="MWMODDIR\RR - Better Meshes"
data="MWMODDIR\ProperlySmoothedMeshes\00 Core"
data="MWMODDIR\ProperlySmoothedMeshes\01 Optional Textures"
data="MWMODDIR\ProperlySmoothedMeshes\03 Higher Poly Glowing Candles"
data="MWMODDIR\ProperlySmoothedMeshes\08 Potted Plant - redware"
# Content
data="MWMODDIR\TamrielData\00 Core"
data="MWMODDIR\TamrielRebuilt\00 Core"
data="MWMODDIR\TamrielRebuilt\01 Faction Integration"
data="MWMODDIR\TamrielRebuilt\03 Travel Network for Core and Vvardenfell"
data="MWMODDIR\LGNPC"           # IMO one of the most crucial mods for Morrowind.
#                               Get the dialog only bundle from http://lgnpc.org/downloads
# Textures / Normal Maps / Meshes
data="MWMODDIR\Intelligent Textures\00 Core"
data="MWMODDIR\Intelligent Textures\01 Atlas Textures"
data="MWMODDIR\Arkitektora Normal Maps\01a Shacks docks and ships"
data="MWMODDIR\Arkitektora Normal Maps\03 Telvanni"
data="MWMODDIR\Arkitektora Normal Maps\04 Daedric"
data="MWMODDIR\Arkitektora Normal Maps\05 Redoran"
data="MWMODDIR\Arkitektora Normal Maps\07 Terrain"
data="MWMODDIR\Arkitektora Normal Maps\08 Rocks"
data="MWMODDIR\Arkitektora Normal Maps\10 Hlaalu"
data="MWMODDIR\Dwemer Normal Mapped"
data="MWMODDIR\OpenMW_AIO_Normals"
data="MWMODDIR\Arkitektora"
data="MWMODDIR\Arkitektora\Hlaalu"
data="MWMODDIR\Arkitektora\Necrom"
data="MWMODDIR\Arkitektora\SewerDirty"
data="MWMODDIR\LyNormals\LysolImperialForts"
data="MWMODDIR\LyNormals\LysolImperialTowns"
data="MWMODDIR\GhastlyGlowyfence"
data="MWMODDIR\R-Zero's Random Replacers - Chimney Smoke"
data="MWMODDIR\Scummy Scum"
data="MWMODDIR\Scum Retexture 1.2 Alternative 2"
data="MWMODDIR\FacesUpscaledWestly"
data="MWMODDIR\WestlysPluginlessHeadAndHairReplacer"
data="MWMODDIR\RobertsBodies"   # People look like people except now Khajiit are busty + topless :/
data="MWMODDIR\RobertsBodies\Dagoth Ur (vanilla shaders)"
data="MWMODDIR\RobertsBodiesColorTweaks\00 Core"
data="MWMODDIR\RobertsBodiesColorTweaks\01 Normal Maps"
data="MWMODDIR\NewBeastBodies33Clean" # Khajiit are no longer topless
data="MWMODDIR\GraphicHerbalism\00 Core + Vanilla Meshes"
data="MWMODDIR\GraphicHerbalism\00 Correct UV Ore + README"
data="MWMODDIR\GraphicHerbalism\01 Optional - Smoothed Meshes"
data="MWMODDIR\WaterfallsTweaks"
# TREES
data="MWMODDIR\Vurt\VurtsAscadianIslesTreeReplacerII"
data="MWMODDIR\Vurt\VurtsBitterCoastTreeII"
data="MWMODDIR\Vurt\VurtsGrazelandTrees"
data="MWMODDIR\Vurt\VurtsLeafyWestGashII"
data="MWMODDIR\Vurt\VurtsLeafyWestGashCompatibilityForTR"
data="MWMODDIR\Vurt\VurtsMournholdTrees"
data="MWMODDIR\Vurt\VurtsSolstheimTreeReplacerII"
# Kind of all over the place from here on out
data="MWMODDIR\SelectedTyddyLandscapes" # I've chosen specific textures here, as I don't like all of them.
data="MWMODDIR\DunmerLanternsReplacer\00 Core"
data="MWMODDIR\DunmerLanternsReplacer\01 Glow Effect"
data="MWMODDIR\DunmerLanternsReplacer\04 Tamriel_Data Patch - Glow Effect"
data="MWMODDIR\DetailedTapestries"
data="MWMODDIR\FadedGlory"       # I did most of the rugs, and none of the tapestries
data="MWMODDIR\SaintsAndTribunalFrescoes"
data="MWMODDIR\Arukinns Better Banners Signs and Signposts"
data="MWMODDIR\LovelyLoadingScreens"
# OK for this next one, I had to select the splash screens from the correct aspect
# ratio, but the menu screen from the weird aspect ratio. YMMV.
data="MWMODDIR\Color Vvardenfell Map Menu+Splash Screens"
data="MWMODDIR\SkeletonsAtlased\01 Improved Better Skulls Textures (Connary)"
data="MWMODDIR\MRM"             # Mountainous Red Mountain.
# I actually don't think this next one is necessary unless you have TR_Preview,
# which I don't recommend (because then you can't update TR)
data="MWMODDIR\DistantMournhold"
data="MWMODDIR\UniqueTavernSignsTRColored"
data="MWMODDIR\SiltStriderWithoutBumpMap"
data="MWMODDIR\SiltStriderNewTextures"
data="MWMODDIR\SiltStriderAnimationRestored"
data="MWMODDIR\GreatService"    # Includes more cut voice lines for vendors
data="MWMODDIR\TelvanniMeshImprovement"
data="MWMODDIR\TRHeightmaps"
data="MWMODDIR\White Suran\White Suran 2 - MD Edition\00 Core"
data="MWMODDIR\White Suran\White Suran 2 - MD Edition\01 White Suran 2 - MD Edition"
data="MWMODDIR\White Suran\White Suran 2 - MD Edition\02 Patch for Vurt's Trees"
data="MWMODDIR\White Suran\White Suran 2 - MD Edition\04 Glow in the Dahrk"
data="MWMODDIR\White Suran\ArkitektoraWhiteSuran"
data="MWMODDIR\PortsOfVvardenfell"
data="MWMODDIR\DetailedDungeons"
data="MWMODDIR\Cave Drips"
data="MWMODDIR\TelvanniLighthouseTelBranora"
data="MWMODDIR\TelvanniLighthouseTelVos"
data="MWMODDIR\Price Balance"
data="MWMODDIR\VenomByteItemLists"
data="MWMODDIR\BewareTheSixthHouse" # Balances this for endgame
data="MWMODDIR\TribunalRebalance"   # Balances this for midgame.
data="MWMODDIR\BloodmoonRebalance"  # Balances this for midgame
data="MWMODDIR\LowerFirstPersonSneakMode"
data="MWMODDIR\BetterClothesComplete"
data="MWMODDIR\Better Clothes Retexture"
data="MWMODDIR\BetterClothesRobBodPatch"
data="MWMODDIR\WeaponSheathing"
data="MWMODDIR\MOP\04 Weapon Sheathing Patch"
data="MWMODDIR\Simply Walking (WS Edition)"
data="MWMODDIR\ExpandedSounds"
data="MWMODDIR\RiseOfHouseTelvanniV152"
data="MWMODDIR\RiseOfHouseTelvanni20"
data="MWMODDIR\UL"              # Uvirith's Legacy
data="MWMODDIR\BUUL"            # Building Up Uvirith's Legacy
data="MWMODDIR\AesthesiaGroundcoverTrim112"
data="MWMODDIR\SkiesIV"
data="MWMODDIR\DyingWorldsMoons"
data="MWMODDIR\GlowingFlames"
data="MWMODDIR\GlowInTheDahrk\00 Core"
data="MWMODDIR\GlowInTheDahrk\02 Interior Sunrays"
data="MWMODDIR\GlowInTheDahrk\04 Telvanni Dormers on Vvardenfell"
data="MWMODDIR\TrueLightsADNecroEdit"
data="MWMODDIR\TrueNightsAndDarknessPSTweaks"
data="MWMODDIR\SubtleCrosshairs"
data="MWMODDIR\SubtleMagicGlow"
data="MWMODDIR"                 # This is where I stick my delta merged plugin
content=Morrowind.esm
content=Tribunal.esm
content=Bloodmoon.esm
content=Patch for Purists.esm
content=Tamriel_Data.esm
content=TR_Mainland.esm
content=MRM.esm
content=Rise of House Telvanni.esm
content=Patch for Purists - Semi-Purist Fixes.ESP
content=Improved Journal Entries.ESP
content=LeFemmArmor.esp
content=EBQ_Artifact.esp
content=Siege at Firemoth.esp
content=master_index.esp
content=AreaEffectArrows.esp
content=adamantiumarmor.esp
content=bcsounds.esp
content=entertainers.esp
content=Expansion Delay.ESP
content=Dubdilla Location Fix.ESP
content=Lake Fjalding Anti-Suck.ESP
content=TR_Factions.esp
content=TR_Travels.esp
content=LGNPC_Merged_Lt.ESP
content=ghastly gg.esp
content=Robert's Bodies.ESP
content=New Khajiit Bodies - Clean.esp
content=correctUV Ore Replacer_respawning.esp
content=Waterfalls Tweaks.esp
content=Vurt's BC Tree Replacer II.ESP
content=Vurt's Grazelands Trees.ESP
content=Vurt's Leafy West Gash.esp
content=aru_signs_en.esp
content=Mournhold LOD.ESP
content=Unique_Tavern_Signs_for_Tamriel_Rebuilt_v2.ESP
content=Silt Strider Animation Restored.ESP
content=Great Service.ESP
content=WhiteSuran2_MD_Edition.esp
content=WhiteSuran2_patch_vurt.esp
# content=Ports Of Vvardenfell V1.6.ESP # mod needs cleaning but automated tools ain't doing it
content=Detailed Dungeons.ESP
content=Cave Drips.ESP
content=RR_Telvanni_Lighthouse_Tel Branora_Eng.ESP
content=RR_Telvanni_Lighthouse_Tel Vos_Eng.ESP
content=Price Balance - Alchemy.ESP
content=Price Balance - Apparatus.ESP
content=Price Balance - Armor.ESP
content=Price Balance - Books.ESP
content=Price Balance - Clothing.ESP
content=Price Balance - Creatures.ESP
content=Price Balance - Crime.ESP
content=Price Balance - Ingredients.ESP
content=Price Balance - Misc Items.ESP
content=Price Balance - Museum.ESP
content=Price Balance - Service.ESP
content=Price Balance - Weapons.ESP
content=vn_economy_ItemLists.esp
content=Ash Vampire Script Fix.esp
content=Sixth House Rebalance.ESP
content=tribunal rebalance.ESP
content=Bloodmoon Rebalance.esp
content=LowerFirstPersonSneak.ESP
content=Better Clothes Complete.ESP
content=Expanded Sounds.esp
content=ROHT_2_0_8.ESP
content=Uvirith's Legacy_3.53.esp
content=UL_3.5_RoHT_1.52_Add-on.esp
content=UL_3.5_TR_16.12_Add-on.esp
content=Building Up Uvirith's Legacy1.1.ESP
content=Glowing Flames - NoMoreLightlessFlames v1.1.ESP
content=Glowing Flames - TrueLightsAndDarkness Tweaks.ESP
content=GITD_Telvanni_Dormers.ESP
content=TLAD Daylight.esp
content=TLAD Lights - Logical Flicker - Necro Colors.esp
content=True Lights and Darkness PS TWEAKS.ESP
content=Glowing_TLAD_Glass_Lantern_Fix.esp
content=delta-merged.omwaddon
content=OMWLLF Mod - 2021-06-13.omwaddon
```
